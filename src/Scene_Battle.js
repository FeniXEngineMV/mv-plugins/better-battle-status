import { _Aliases, _Params } from './Core'

_Aliases.sceneBattleRefresh = Scene_Battle.prototype.update
Scene_Battle.prototype.update = function () {
  _Aliases.sceneBattleRefresh.call(this)
  if (this._statusWindow) {
    this._statusWindow.refreshPortraits()
  }
}

_Aliases.createActorCommandWindow = Scene_Battle.prototype.createActorCommandWindow
Scene_Battle.prototype.createActorCommandWindow = function () {
  _Aliases.createActorCommandWindow.call(this)
  const yOffset = _Params.actorCommandOptions.yOffset
  const xOffset = _Params.actorCommandOptions.xOffset
  this._actorCommandWindow.y = this._statusWindow.y - this._actorCommandWindow.height + yOffset
  this._actorCommandWindow.x = 0 + xOffset
}

_Aliases.createPartyCommandWindow = Scene_Battle.prototype.createPartyCommandWindow
Scene_Battle.prototype.createPartyCommandWindow = function () {
  _Aliases.createPartyCommandWindow.call(this)
  this._partyCommandWindow.y = this._statusWindow.y - this._partyCommandWindow.height
}

_Aliases.createDisplayObjects = Scene_Battle.prototype.createDisplayObjects
Scene_Battle.prototype.createDisplayObjects = function () {
  _Aliases.createDisplayObjects.call(this)
  this.hideAllActorBattlers()
}

Scene_Battle.prototype.hideAllActorBattlers = function () {
  this._spriteset._actorSprites.forEach(actor => {
    actor.visible = false
    actor.opacity = 0
  })
}

Scene_Battle.prototype.updateWindowPositions = function () {
  if (BattleManager.isInputting()) {
    const xOffset = _Params.actorCommandOptions.xOffset
    const currentActorRect = this._statusWindow.currentActorRect()

    this._actorCommandWindow.x = currentActorRect.x - xOffset
  }
}
