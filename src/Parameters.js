/**
 * All plugin parameters and structs for this plugin.
 *
 * @file parameters
 *
 * @author       FeniXEngine Contributors
 * @copyright    2018 FeniX Engine
 */

/*:
 * @pluginname X_BetterBattleStatus
 * @plugindesc Improve the UI of the status window in frontview battles <X_BetterBattleStatus>
 * @modulename X_BetterBattleStatus
 * @required
 * @external
 *
 * @param portraitOptions
 * @text Portrait Options
 * @type struct<SpriteOptions>
 * @desc The options for the actor portraits
 * @default {"xOffset":"0.0","yOffset":"-77","xScale":"0.6","yScale":"0.6","yAnchor":"0.0","enableDeathTone":"true","deathTone":"181, 51, 0, 255"}
 *
 * @param damageOptions
 * @text Damage Popup Options
 * @type struct<OffsetOptions>
 * @desc The options for animations and damage display
 * @default {"xOffset":"-25","yOffset":"-90"}
 *
 * @param actorOptions
 * @text Actor Options
 * @type struct<OffsetOptions>
 * @desc The options for each actor, which is responsible for where animations and damage popups are located (See Help)
 * @default {"xOffset":"-10","yOffset":"-150"}
 *
 * @param actorCommandOptions
 * @text Actor Command Window
 * @type struct<WindowCommand>
 * @desc The options for the actor command window
 * @default {"xOffset":"0","yOffset":"-50","maxCols":"2","visibleRows":"2"}
 *
 * @param selectType
 * @text Active Selection Type
 * @type select
 * @option Rectangle
 * @value rectangle
 * @option Sprite Tone
 * @value tone
 * @desc The type of selection cue, rectangle is default cursor and sprite tone
 * will allow portrait's color tone to change
 * @default tone
 *
 * @param selectTone
 * @text Select Sprite Tone
 * @type text
 * @desc The color tone to change the portrait when it's the active actor
 * @default 11, 130, 37, 80
 *
 * @param statusIconsOptions
 * @text Status Icons Options
 * @desc Change the offset of the status icons
 * @type struct<OffsetOptions>
 * @default {"xOffset":"0","yOffset":"0"}
 *
 * @param disableGauges
 * @text Disable Gauges
 * @desc Disable the drawing of actor gauges
 * @type boolean
 * @default true
 *
 * @author FeniX Contributors (https://fenixenginemv.gitlab.io/)
 *
 * @help
--------------------------------------------------------------------------------
 # TERMS OF USE

 MIT License -

 * Free for use in any RPG Maker MV game project, commercial or otherwise

 * Credit may go to FeniXEngine Contributors or FeniXEngine

 * Though not required, you may provide a link back to the original source code,
   repository or website.

 -------------------------------------------------------------------------------
  # INSTALLATION

  Place the plugin file directly in your game project's `/js/plugins/`
  directory

  Actor portraits are to be named the same as the original characters face
  filename + the index with a suffix of _Battle

  Example:
  Actor 1 uses the 3rd face from Actor_1 file so we place our portrait in
  img/pictures like so

  img/pictures/Actor_1_3_Battle

 -------------------------------------------------------------------------------
 # INFORMATION

 This plugin changes the way the battle status window is displayed in frontview
 battles.
 It draws actor portraits based on the actors face name and index and draws
 them in battle. Each portrait will act as a normal sprite, meaning enemy attack
 animations will be displayed on the actor portrait.

 -------------------------------------------------------------------------------
 # PARAMETERS

 The plugin's parameters provides numerous position options for both the actor
 command window and the portrait sprites.

 A few key parameters are the "Death Tone" within the portrait options. This
 color tone is used to change the color of the portrait when the actor's hp is
 at 0.

 You can choose two types of cursor or selection cues that lets the player know
 which actor is currently selected. Aside from the command window lining up with
 the portrait you may also choose to use the default rectangle cursor or change
 the color tone of the portrait.

 Actor Options
 The actor options are important to understand. The way BetterBattleStatus plays
 animations and damage popups is actually by enabling side view sprites in
 frontview battles. The actor sprites are hidden and placed by the battle
 status window in the appropriate location. So when an enemy attacks the player
 they're actually attacking the hidden sprites. So if animations and damage
 popups are not correctly displaying you may consider adjusting the actor
 options.

*/
/* eslint-disable spaced-comment */

/*~struct~SpriteOptions:
*
* @param xOffset
* @text X Offset
* @desc The default x position offset for sprite
* @type number
* @max 9999
* @min -9999
* @default 0
*
* @param yOffset
* @text Y Offset
* @desc The default y position offset for sprite
* @type number
* @max 9999
* @min -9999
* @default -75
*
* @param xScale
* @text Scale X
* @desc The default sprite scaling for x
* @type number
* @max 9999
* @min 0
* @decimals 1
* @default 1
*
* @param yScale
* @text Scale Y
* @desc The default sprite scaling for y
* @type number
* @max 9999
* @min 0
* @decimals 1
* @default 1
*
* @param yAnchor
* @text Anchor Y
* @desc The default sprite y anchor
* @type number
* @max 9999
* @min 0
* @decimals 1
* @default 0
*
* @param enableDeathTone
* @text Enable Death Tone
* @desc Enable the death tone which changes the sprite to a color tone when
* actor is dead
* @type boolean
* @default true
*
* @param deathTone
* @text Death Tone
* @desc Change sprite to this color tone when actor is dead
* @type text
* @default 181, 51, 0, 255
*/

/*~struct~OffsetOptions:
*
* @param xOffset
* @text X Offset
* @desc The default x offset
* @type number
* @max 9999
* @min -9999
* @default 0
*
* @param yOffset
* @text Y Offset
* @desc The default y offset
* @type number
* @max 9999
* @min -9999
* @default 0
*
*/

/*~struct~WindowCommand:
*
* @param xOffset
* @text X Offset
* @desc The default x position offset for command window
* @max 9999
* @min -9999
* @type number
* @default 0
*
* @param yOffset
* @text Y Offset
* @desc The default y position offset for command window
* @max 9999
* @min -9999
* @type number
* @default 0
*
* @param maxCols
* @text Max Cols
* @desc The default max columns for command window
* @type number
* @default 2
*
* @param visibleRows
* @text Visible Rows
* @desc The default visible rows for command window
* @type number
* @default 2
*/
