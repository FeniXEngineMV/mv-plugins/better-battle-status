import { _Params } from './Core'

Sprite_Actor.prototype.setActorHome = function (index) {
  const multiplier = Graphics.boxWidth / $gameParty.battleMembers().length
  const y = Graphics.boxHeight + _Params.actorOptions.yOffset
  if (index === 0) {
    this.setHome(0, y)
  } else {
    this.setHome(0 + index * (multiplier + _Params.actorOptions.xOffset), y)
  }
}

Sprite_Actor.prototype.moveToStartPosition = function () {
  this.startMove(150, 0, 0)
}

Sprite_Actor.prototype.damageOffsetX = function () {
  return _Params.damageOptions.xOffset
}

Sprite_Actor.prototype.damageOffsetY = function () {
  return _Params.damageOptions.yOffset
}
