import { _Params, _Aliases } from './Core'

_Aliases.windowBattleStatusInitialize = Window_BattleStatus.prototype.initialize
Window_BattleStatus.prototype.initialize = function () {
  this._spritePortraits = []

  _Aliases.windowBattleStatusInitialize.call(this)
}

Window_BattleStatus.prototype.updateCursor = function () {
  const actor = $gameParty.battleMembers()[this.index()]
  const sprite = this._spritePortraits.filter(sprite => actor && sprite.name === actor.name())[0]
  const rect = this.itemRect(this.index())
  const allRowsHeight = this.maxRows() * this.itemHeight()

  this._spritePortraits.forEach(sprite => sprite.setColorTone([0, 0, 0, 0]))

  if (this._cursorAll) {
    if (_Params.selectType === 'tone') {
      this._spritePortraits.forEach(sprite => sprite.setColorTone(_Params.selectTone.split(',')))
    } else {
      this.setCursorRect(0, 0, this.contents.width, allRowsHeight)
      this.setTopRow(0)
    }
  } else if (this.isCursorVisible()) {
    if (_Params.selectType === 'tone') {
      sprite.setColorTone(_Params.selectTone.split(','))
    } else {
      this.setCursorRect(rect.x, rect.y, rect.width, this.lineHeight())
    }
  } else {
    this.setCursorRect(0, 0, 0, 0)
    this._spritePortraits.forEach(sprite => sprite.setColorTone([0, 0, 0, 0]))
  }
}

Window_BattleStatus.prototype.currentActorRect = function () {
  return this.itemRect(this.index())
}

Window_BattleStatus.prototype.windowWidth = function () {
  return Graphics.boxWidth
}

Window_BattleStatus.prototype.windowHeight = function () {
  return this.fittingHeight(4)
}

Window_BattleStatus.prototype.itemHeight = function (index) {
  return this.contentsHeight()
}

Window_BattleStatus.prototype.numVisibleRows = function (index) {
  return 1
}

Window_BattleStatus.prototype.maxPageRows = function (index) {
  return 1
}

Window_BattleStatus.prototype.maxCols = function () {
  return $gameParty.battleMembers().length
}

Window_BattleStatus.prototype.maxItems = function () {
  return $gameParty.battleMembers().length
}

Window_BattleStatus.prototype.refresh = function () {
  this.contents.clear()
  this.drawAllItems()
}

Window_BattleStatus.prototype.drawActorHp = function (actor, x, y, width) {
  width = width || 186

  if (!_Params.disableGauges) {
    const color1 = this.hpGaugeColor1()
    const color2 = this.hpGaugeColor2()
    this.drawGauge(x, y, width, actor.hpRate(), color1, color2)
  }

  this.changeTextColor(this.systemColor())
  this.drawText(TextManager.hpA, x, y, 44)
  this.drawCurrentAndMax(actor.hp, actor.mhp, x, y, width,
    this.hpColor(actor), this.normalColor())
}

Window_BattleStatus.prototype.drawActorMp = function (actor, x, y, width) {
  width = width || 186

  if (!_Params.disableGauges) {
    const color1 = this.mpGaugeColor1()
    const color2 = this.mpGaugeColor2()
    this.drawGauge(x, y, width, actor.mpRate(), color1, color2)
  }

  this.changeTextColor(this.systemColor())
  this.drawText(TextManager.mpA, x, y, 44)
  this.drawCurrentAndMax(actor.mp, actor.mmp, x, y, width,
    this.mpColor(actor), this.normalColor())
}

Window_BattleStatus.prototype.drawActorTp = function (actor, x, y, width) {
  width = width || 96

  if (!_Params.disableGauges) {
    const color1 = this.tpGaugeColor1()
    const color2 = this.tpGaugeColor2()
    this.drawGauge(x, y, width, actor.tpRate(), color1, color2)
  }

  this.changeTextColor(this.systemColor())
  this.drawText(TextManager.tpA, x, y, 44)
  this.changeTextColor(this.tpColor(actor))
  this.drawText(actor.tp, x + width - 64, y, 64, 'right')
}

Window_BattleStatus.prototype.drawActorBustImage = function (actor, x, y) {
  // If sprites already exist return so we don't create more
  if (this._spritePortraits.length >= $gameParty.battleMembers().length) {
    return
  }

  const faceName = actor.faceName()
  const faceIndex = actor.faceIndex()
  const bitmap = ImageManager.loadPicture(`${faceName}_${faceIndex}_Battle`)
  const sprite = new Sprite(bitmap)
  bitmap.addLoadListener(bitmap => {
    const options = _Params.portraitOptions

    if (actor.isDead()) {
      sprite.setColorTone(options.deathTone.split(','))
    }

    sprite.name = actor.name()
    sprite.scale.x = options.xScale
    sprite.scale.y = options.yScale
    sprite.x = x + options.xOffset
    sprite.y = y + options.yOffset
    sprite.anchor.y = options.yAnchor
    this._spritePortraits.push(sprite)
    this.addChildToBack(sprite)
  })
}

Window_BattleStatus.prototype.refreshPortraits = function () {
  const options = _Params.portraitOptions

  if (options.enableDeathTone) {
    this._spritePortraits.forEach(sprite => {
      const actor = $gameParty.battleMembers().filter(actor => actor.name() === sprite.name)[0]

      if (actor && actor.hp <= 0) {
        sprite.setColorTone(options.deathTone.split(','))
      }
    })
  }
}

Window_BattleStatus.prototype.drawItem = function (index) {
  const actor = $gameParty.battleMembers()[index]
  const nameTextWidth = this.textWidth(actor.name())
  const statusIconsOptions = _Params.statusIconsOptions
  const stateIconsWidth = Window_Base._iconWidth + statusIconsOptions.xOffset
  const rect = this.itemRect(index)

  if (actor) {
    const x = rect.x
    let y = rect.y
    const lineHeight = this.lineHeight()

    if (!$dataSystem.optDisplayTp) {
      y += lineHeight
    }

    this.drawActorName(actor, x + 20, y)
    this.drawActorIcons(actor, x + nameTextWidth + stateIconsWidth, y + statusIconsOptions.yOffset)
    this.drawActorHp(actor, x + 15, y + lineHeight * 1, rect.width - 30)
    this.drawActorMp(actor, x + 15, y + lineHeight * 2, rect.width - 30)
    this.drawActorBustImage(actor, x, y)
    if ($dataSystem.optDisplayTp) {
      this.drawActorTp(actor, x + 15, y + lineHeight * 3, rect.width - 30)
    }
  }
}
