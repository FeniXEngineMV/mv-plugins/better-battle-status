/**
 * The Core file is responsible for fetching and parsing of all plugin
 * parameters, note-tags and other important information. The core should
 * contain all global variables and base configuration related to the plugin.
 *
 * @file Core
 *
 * @author       FeniXEngine Contributors
 * @copyright    2018 FeniXEngine
 */

import { convertParameters } from 'fenix-tools'

// Plugin global API
const rawParameters = $plugins.filter(
  plugin => plugin.description.contains('<X_BetterBattleStatus>'))[0].parameters
export const _Params = convertParameters(JSON.stringify(rawParameters))

export const _Aliases = {}
