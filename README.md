# Better Battle Status

## What does this plugin do ?
This plugin changes the way the battle status window is displayed in frontview battles. It draws actor portraits based on the actors face name and index and draws them in battle. Each portrait will act as a normal sprite, meaning enemy attack animations and damage popups will be displayed on the actor portrait.

## Parameters
The plugin's parameters provides numerous position options for both the actor command window and the portrait sprites.

A few key parameters are the "Death Tone" within the portrait options. This color tone is used to change the color of the portrait when the actor's hp is at 0.

You can choose two types of cursor or selection cues that lets the player know which actor is currently selected. Aside from the command window lining up with the portrait you may also choose to use the default rectangle cursor or change the color tone of the portrait.

## Terms Of Use
* All plugin under the FeniXEngineMV name are MIT License
* Free for use in any RPG Maker MV game project, commercial or otherwise
* Credit may go to FeniXEngine Contributors or FeniXEngine but is optional
* Though not required, you may provide a link back to the original source code, repository or website.